# NewsProject

About the APP

The aim of this application is create/display Feed off news via API request, give user a possibility to:

• search an item via keyword
• save an item in local cache for offline viewing from saved collection
• delete an item from saved items

 App written in the Kotlin by using MVVM with Clean architecture, also used Navigation component(Single
 activity).
 As a network client used Retrofit.

 Also used following libs:

 • Koin
 • Coroutines
 • Coil
 • Room