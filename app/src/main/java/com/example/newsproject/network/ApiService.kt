package com.example.newsproject.network

import com.example.newsproject.data.models.NewsModel
import com.example.newsproject.SEARCH_END_POINT
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {
    @GET(SEARCH_END_POINT)
    suspend fun getData() : Response<NewsModel>
}