package com.example.newsproject.usecases

import androidx.lifecycle.LiveData
import com.example.newsproject.data.models.NewsModel
import com.example.newsproject.data.repos.NewsRepository
import com.example.newsproject.database.MainModel
import retrofit2.Response

class NewsUseCaseImpl(private val newsRepository: NewsRepository): NewsUseCase {
    override suspend fun getAllData(): Response<NewsModel> {
        return newsRepository.getAllData()
    }

    override suspend fun saveNews(mainModel: MainModel) {
        return newsRepository.saveNews(mainModel)
    }

    override fun getSavedNews(): LiveData<List<MainModel>> {
        return  newsRepository.getSavedNews()
    }

    override suspend fun deleteNews(mainModel: MainModel) {
        return newsRepository.deleteNews(mainModel)
    }

}