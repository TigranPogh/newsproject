package com.example.newsproject.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface NewsDAO {
    @get:Query("SELECT * FROM dao_model")
    val all: LiveData<List<MainModel>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(mainModel: MainModel?)

    @Delete
    suspend fun delete(mainModel: MainModel?)
}