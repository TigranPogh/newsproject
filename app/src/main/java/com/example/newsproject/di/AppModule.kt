package com.example.newsproject

import android.app.Application
import androidx.room.Room
import com.example.newsproject.database.AppDatabase
import com.example.newsproject.database.NewsDAO
import com.example.newsproject.network.ApiService
import com.example.newsproject.data.repos.NewsRepository
import com.example.newsproject.data.repos.NewsRepositoryImpl
import com.example.newsproject.usecases.NewsUseCase
import com.example.newsproject.usecases.NewsUseCaseImpl
import com.example.newsproject.view.viewmodels.NewsViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private fun provideOkHttpClient() = if (BuildConfig.DEBUG) {
    val loggingInterceptor = HttpLoggingInterceptor()
    loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
    OkHttpClient.Builder()
        .addInterceptor(loggingInterceptor)
        .build()
} else OkHttpClient
    .Builder()
    .build()

private fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit =
    Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .build()

private fun provideApiService(retrofit: Retrofit): ApiService =
    retrofit.create(ApiService::class.java)

private fun provideDatabase(application: Application) : AppDatabase {
    return  Room.databaseBuilder(application, AppDatabase::class.java, "database").build()
}

private fun provideDao(appDatabase: AppDatabase) : NewsDAO? {
    return appDatabase.newsDAO()
}

val appModule = module {
    single { provideOkHttpClient() }
    single { provideRetrofit(get()) }
    single { provideApiService(get()) }

    single { provideDatabase(androidApplication())}
    single { provideDao(get())}
}

val repoModule = module {
    single<NewsRepository> {
        return@single NewsRepositoryImpl(get(), get())
    }
}

val viewModelModule = module {
    viewModel {
        NewsViewModel(get())
    }
}

val useCaseModule = module {
    single <NewsUseCase> {
        return@single NewsUseCaseImpl(get())
    }
}
