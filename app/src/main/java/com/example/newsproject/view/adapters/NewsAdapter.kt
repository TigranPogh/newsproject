package com.example.newsproject.view.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.newsproject.data.models.DataItem
import com.example.newsproject.R
import com.example.newsproject.database.MainModel
import kotlinx.android.synthetic.main.news_item.view.*
import java.util.*

class NewsAdapter(private val itemClick: ((MainModel) -> Unit)? = null) :
    RecyclerView.Adapter<NewsAdapter.NewsHolder>(), Filterable {

    var list: List<DataItem?>? = emptyList()
    var newsFilterList: List<DataItem?>? = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.news_item, parent, false)
        return NewsHolder(v)
    }

    override fun onBindViewHolder(holder: NewsHolder, position: Int) {
        newsFilterList?.get(position)?.let { holder.bind(it, itemClick ?: { _ -> print("clicked") }) }
    }

    override fun getItemCount(): Int {
        return newsFilterList!!.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(list: List<DataItem?>?) {
        this.list = list
        newsFilterList = list
        notifyDataSetChanged()
    }

    class NewsHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        private lateinit var title: TextView
        private lateinit var img: ImageView

        init {
            findViews(itemView)
        }

        private fun findViews(view: View) {
            title = view.findViewById(R.id.newsTitle)
            img = view.findViewById(R.id.imageId)
        }

        fun bind(item: DataItem, itemClick: ((MainModel) -> Unit)) {
            val mainModel = item.title?.let { item.content?.let { it1 ->
                MainModel(it,
                    it1, item.imageUrl)
            } }
            itemView.imgDelete.visibility = GONE
            itemView.setOnClickListener {
                if (mainModel != null) {
                    itemClick(mainModel)
                }
            }

            title.text = item.title
            img.load(item.imageUrl)
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(p0: CharSequence?): FilterResults {
                val charSearch = p0.toString()
                newsFilterList = if (charSearch.isEmpty()) {
                    list
                } else {
                    val resultList: List<DataItem?> = emptyList()
                    val newResultList = resultList.toMutableList()
                    for (row in list!!) {
                        if (row != null) {
                            if (row.title?.lowercase(Locale.ROOT)
                                    ?.contains(charSearch.lowercase(Locale.ROOT)) == true
                            ) {
                                newResultList.add(row)
                            }
                        }
                    }
                    newResultList
                }
                val filterResults = FilterResults()
                filterResults.values = newsFilterList
                return filterResults
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                newsFilterList = p1?.values as List<DataItem?>?
                notifyDataSetChanged()
            }
        }
    }
}