package com.example.newsproject.view.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.newsproject.data.models.NewsModel
import com.example.newsproject.database.MainModel
import com.example.newsproject.usecases.NewsUseCase
import kotlinx.coroutines.launch

class NewsViewModel(private val newsUseCase: NewsUseCase) : ViewModel() {
    private var newsMutableLiveData = MutableLiveData<NewsModel>()
    var item = MainModel("", "", "")
    var isFavorites = false

    fun getNews() = viewModelScope.launch {
        try {
            val response = newsUseCase.getAllData()
            if(response.isSuccessful && response.body() != null) {
                newsMutableLiveData.value = response.body()
            }
        } catch (ex : Exception) {
            Log.e("tagaag", ex.message!!)
        }
    }

    fun getNewsLiveData() : LiveData<NewsModel> = newsMutableLiveData

    fun saveNews(mainModel: MainModel) = viewModelScope.launch {
        newsUseCase.saveNews(mainModel)
    }

    fun deleteNews(mainModel: MainModel) = viewModelScope.launch {
        newsUseCase.deleteNews(mainModel)
    }

    fun getSavedNewsLiveData() : LiveData<List<MainModel>> = newsUseCase.getSavedNews()
}