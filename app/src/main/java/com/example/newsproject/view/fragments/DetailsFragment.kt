package com.example.newsproject.view.fragments

import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.fragment.app.Fragment
import coil.load
import com.example.newsproject.database.MainModel
import com.example.newsproject.R
import com.example.newsproject.databinding.FragmentDetailsBinding
import com.example.newsproject.view.viewmodels.NewsViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class DetailsFragment : Fragment(R.layout.fragment_details) {

    private var binding: FragmentDetailsBinding? = null
    private val viewModel: NewsViewModel by sharedViewModel()
    private lateinit var mainModel: MainModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentDetailsBinding.bind(view)

        initView()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun onSaveClicked() {
        viewModel.saveNews(viewModel.item)
        Toast.makeText(requireActivity(), "Saved for offline reading", Toast.LENGTH_SHORT).show()
    }

    private fun initView() {
        mainModel = viewModel.item
        binding?.txtTitle?.text = mainModel.title
        binding?.txtContent?.text = mainModel.text
        binding?.img?.load(mainModel.img)
        binding?.imgSave?.setOnClickListener {
            onSaveClicked()
        }
        if (viewModel.isFavorites) {
            binding?.imgSave?.visibility = GONE
        } else {
            binding?.imgSave?.visibility = VISIBLE
        }
    }
}