package com.example.newsproject.view.fragments

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newsproject.R
import com.example.newsproject.databinding.FragmentNewsBinding
import com.example.newsproject.view.adapters.NewsAdapter
import com.example.newsproject.view.adapters.SavedNewsAdapter
import com.example.newsproject.view.viewmodels.NewsViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class NewsFragment : Fragment(R.layout.fragment_news) {

    private var binding: FragmentNewsBinding? = null
    private val viewModel: NewsViewModel by sharedViewModel()
    private lateinit var newsAdapter: NewsAdapter
    private lateinit var savedNewsAdapter: SavedNewsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentNewsBinding.bind(view)

        initSearchView()
        initNavView()
    }

    override fun onStart() {
        super.onStart()

        if (viewModel.isFavorites) {
            initSavedNewsAdapter()
            initSavedNewsObserver()
        } else {
            initNewsAdapter()
            viewModel.getNews()
            initObservers()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun initObservers() {
        viewModel.getNewsLiveData().observe(requireActivity(), {result ->
            newsAdapter.setItems(result?.data)
            binding?.progressBar?.visibility = GONE
        })
    }

    private fun initSavedNewsObserver() {
        viewModel.getSavedNewsLiveData().observe(requireActivity(), {savedNewsList ->
            savedNewsAdapter.setItems(savedNewsList)
            binding?.progressBar?.visibility = GONE
        })
    }

    private fun initNewsAdapter() {
        newsAdapter = NewsAdapter{ mainModel ->
            viewModel.item = mainModel
            Navigation.findNavController(requireView()).navigate(R.id.action_newsFragment_to_detailsFragment)
        }
        binding?.rvNews?.adapter = newsAdapter
        binding?.rvNews?.layoutManager = LinearLayoutManager(requireActivity())
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initSavedNewsAdapter() {
        savedNewsAdapter = SavedNewsAdapter  ({mainModel ->
            viewModel.deleteNews(mainModel)
            savedNewsAdapter.notifyDataSetChanged()
        },
            { mainModel ->
            viewModel.item = mainModel
            Navigation.findNavController(requireView()).navigate(R.id.action_newsFragment_to_detailsFragment)
        })

        binding?.rvNews?.adapter = savedNewsAdapter
        binding?.rvNews?.layoutManager = LinearLayoutManager(requireActivity())
    }

    private fun initSearchView() {
        val searchIcon = binding?.newsSearch?.findViewById<ImageView>(R.id.search_mag_icon)
        searchIcon?.setColorFilter(Color.WHITE)


        val cancelIcon = binding?.newsSearch?.findViewById<ImageView>(R.id.search_close_btn)
        cancelIcon?.setColorFilter(Color.WHITE)

        val textView = binding?.newsSearch?.findViewById<TextView>(R.id.search_src_text)
        textView?.setTextColor(Color.WHITE)

        binding?.newsSearch?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (viewModel.isFavorites) {
                    savedNewsAdapter.filter.filter(newText)
                }
                newsAdapter.filter.filter(newText)
                return false
            }

        })
    }

    private fun initNavView() {
        binding?.navView?.setOnItemSelectedListener { item: MenuItem ->
            binding?.newsSearch?.setQuery("", false)
            binding?.newsSearch?.clearFocus()
            when (item.itemId) {
                R.id.bottomNavigationNews -> {
                    initNewsAdapter()
                    viewModel.getNews()
                    initObservers()
                    viewModel.isFavorites = false
                    true
                }
                R.id.bottomNavigationLiked -> {
                    initSavedNewsAdapter()
                    initSavedNewsObserver()
                    viewModel.isFavorites = true
                    true
                }
                else -> false
            }
        }
    }
}