package com.example.newsproject.view.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.newsproject.R
import com.example.newsproject.database.MainModel
import kotlinx.android.synthetic.main.news_item.view.*
import java.util.*


class SavedNewsAdapter(
    private val deleteClick: ((MainModel) -> Unit)? = null,
    private val itemClick: ((MainModel) -> Unit)? = null
) :
    RecyclerView.Adapter<SavedNewsAdapter.SavedNewsHolder>(), Filterable {

    private var list: List<MainModel> = emptyList()
    var newsFilterList: List<MainModel> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavedNewsHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.news_item, parent, false)
        return SavedNewsHolder(v)
    }

    override fun onBindViewHolder(holder: SavedNewsHolder, position: Int) {
        holder.bind(
            newsFilterList[position],
            itemClick ?: { _ -> print("Clicked") },
            deleteClick ?: { _ -> print("Clicked") })
    }

    override fun getItemCount(): Int {
        return newsFilterList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(list: List<MainModel>) {
        this.list = list
        newsFilterList = list
        notifyDataSetChanged()
    }

    class SavedNewsHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        private lateinit var title: TextView
        private lateinit var img: ImageView

        init {
            findViews(itemView)
        }

        private fun findViews(view: View) {
            title = view.findViewById(R.id.newsTitle)
            img = view.findViewById(R.id.imageId)
        }

        fun bind(
            mainModel: MainModel,
            itemClick: ((MainModel) -> Unit),
            deleteClick: (MainModel) -> Unit
        ) {
            itemView.setOnClickListener { itemClick(mainModel) }
            itemView.imgDelete.visibility = VISIBLE
            itemView.imgDelete.setOnClickListener { deleteClick(mainModel) }

            title.text = mainModel.title
            img.load(mainModel.img)
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(p0: CharSequence?): FilterResults {
                val charSearch = p0.toString()
                newsFilterList = if (charSearch.isEmpty()) {
                    list
                } else {
                    val resultList: List<MainModel> = emptyList()
                    val newResultList = resultList.toMutableList()
                    for (row in list) {
                        if (row.title.lowercase(Locale.ROOT)
                                .contains(charSearch.lowercase(Locale.ROOT))
                        ) {
                            newResultList.add(row)
                        }
                    }
                    newResultList
                }
                val filterResults = FilterResults()
                filterResults.values = newsFilterList
                return filterResults
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                newsFilterList = p1?.values as List<MainModel>
                notifyDataSetChanged()
            }
        }
    }
}