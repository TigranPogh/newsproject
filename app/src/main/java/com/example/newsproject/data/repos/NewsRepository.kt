package com.example.newsproject.data.repos

import androidx.lifecycle.LiveData
import com.example.newsproject.data.models.NewsModel
import com.example.newsproject.database.MainModel
import retrofit2.Response

interface NewsRepository {

    suspend fun getAllData() : Response<NewsModel>

    suspend fun saveNews(mainModel: MainModel)

    fun getSavedNews(): LiveData<List<MainModel>>

    suspend fun deleteNews(mainModel: MainModel)
}