package com.example.newsproject.data.repos

import androidx.lifecycle.LiveData
import com.example.newsproject.data.models.NewsModel
import com.example.newsproject.database.MainModel
import com.example.newsproject.database.NewsDAO
import com.example.newsproject.network.ApiService
import retrofit2.Response

class NewsRepositoryImpl(
    private val apiService: ApiService,
    private val newsDAO: NewsDAO
) : NewsRepository {

    override suspend fun getAllData(): Response<NewsModel> {
        return apiService.getData()
    }

    override suspend fun saveNews(mainModel: MainModel) {
        newsDAO.insert(mainModel)
    }

    override fun getSavedNews(): LiveData<List<MainModel>> = newsDAO.all

    override suspend fun deleteNews(mainModel: MainModel) {
        newsDAO.delete(mainModel)
    }
}